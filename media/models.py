from django.conf import settings
from django.db import models
from django.urls import reverse





class Media(models.Model):
	EffSection = models.CharField(max_length=50)	
	IHSection = models.CharField(max_length=50)		
	BegSection = models.CharField(max_length=50)	
	Efficiency = models.IntegerField(blank=True, null=True)	
	InformationHierarchy = models.IntegerField(blank=True, null=True)	
	Beginner = models.IntegerField(blank=True, null=True)	
	author = models.ForeignKey(
		settings.AUTH_USER_MODEL,
		on_delete=models.CASCADE,
	)
	title = models.CharField(max_length=200)	
	media_url = models.CharField(max_length=280)
	info = models.CharField(max_length=2800)	
	date = models.DateTimeField(auto_now_add=True)
	
	

	
	
	def get_absolute_url(self):

		return reverse('media_detail', args=[str(self.id)])

	def __str__(self):
       		return self.title

	
