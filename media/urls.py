from django.urls import path
from . import views

urlpatterns = [
	path('<int:pk>/', views.MediaDetailView.as_view(), name='media_detail'),
	path('', views.MediaListView.as_view(), name='media_list'),
	
]
	



