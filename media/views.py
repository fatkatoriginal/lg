from django.views.generic import ListView, DetailView
from django.urls import reverse_lazy
from django.utils import timezone
from django.core.paginator import Paginator


from . import models


class MediaListView(ListView):
	model = models.Media
	template_name = 'media_list.html'
	paginate_by = 5  # if pagination is desired
	
	
	


	
class MediaDetailView(DetailView):
	model = models.Media
	template_name = 'media_detail.html'



class bMediaListView(ListView):
	model = models.Media
	template_name = 'bmedia_list.html'
	paginate_by = 5  # if pagination is desired
	
	def get_queryset(self):
        	return models.Media.objects.order_by('Beginner')
	
class eMediaListView(ListView):
	model = models.Media
	template_name = 'emedia_list.html'
	paginate_by = 5  # if pagination is desired
	
	def get_queryset(self):
        	return models.Media.objects.order_by('Efficiency')


class ihMediaListView(ListView):
	model = models.Media
	template_name = 'ihmedia_list.html'
	paginate_by = 5  # if pagination is desired
	
	def get_queryset(self):
        	return models.Media.objects.order_by('InformationHierarchy')


class lvMediaListView(ListView):
	model = models.Media
	template_name = 'lvmedia_list.html'
	paginate_by = 5  # if pagination is desired
	
	def get_queryset(self):
        	return models.Media.objects.filter(EffSection='LV').order_by('-Efficiency')
